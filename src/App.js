import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

import FilmGridContainer from './grid/FilmGridContainer';

class App extends Component {
  render() {
    return (
      <div className='App'>
        <div className='App-header'>
          <img src={logo} className='App-logo' alt='logo' />
          <h2>Make movies great again</h2>
        </div>
        <FilmGridContainer />

      </div>
    );
  }
}

export default App;
