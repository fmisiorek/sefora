import React, { Component } from 'react';
import './Range.css';

class Range extends Component {
    constructor() {
        super();
        this.handleChangeFrom = this.handleChangeFrom.bind(this);
        this.handleChangeTo = this.handleChangeTo.bind(this);
        this.state = { from: null, to: null };
    }

    handleChangeFrom(e) {
        this.setState({ from: e.target.value }, this.callRangeChanged);
    }

    handleChangeTo(e) {
        this.setState({ to: e.target.value } , this.callRangeChanged);
    }

    callRangeChanged() {
        this.props.range(this.props.field, this.state.from, this.state.to);
    }

    render() {
        return (
            <div className='Range'>
                <label>{this.props.name} from:
                    <input type='number' onChange={this.handleChangeFrom} />
                </label>
                <label> to:
                    <input type='number' onChange={this.handleChangeTo} />
                </label>
            </div>
        );
    }
}

export default Range;