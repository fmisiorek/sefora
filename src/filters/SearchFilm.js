import React, { Component } from 'react';
import './SearchFilm.css';

class SearchFilm extends Component {
    constructor() {
        super();
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(e) {
        this.props.search(e.target.value);
    }

    render() {
        return (
            <div className='SearchFilm'>
                <label>Search film:
                <input type='text' onChange={this.handleChange} />
                </label>
            </div>
        );
    }
}

export default SearchFilm;