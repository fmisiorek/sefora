import React, { Component } from 'react';
import './SortLink.css';

class SortLink extends Component {
    constructor() {
        super();
        this.handleClick = this.handleClick.bind(this);
    }

    handleClick(e) {
        // console.log('click na', e, this.props);
        this.props.sort(this.props.value);
    }

    render() {
        return (
            <a className='SortLink' onClick={this.handleClick}>
                {this.props.title}
            </a>
        );
    }
}

export default SortLink;