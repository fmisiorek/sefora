import React, { Component } from 'react';
import SortLink from './SortLink';
import './ListHeader.css';

class ListHeader extends Component {
    renderArrow(asc) {
        return asc === true ? '\u2191' : '\u2193';
    }

    render() {
        return (
            <thead className='ListHeader'>
                <tr>
                    {this.props.headers.map((header) => {
                        return <td key={header.id}><SortLink value={header.field} title={header.name} sort={this.props.sort} />{this.props.sortBy.field === header.field ? this.renderArrow(this.props.sortBy.asc) : ''}</td>
                    })}
                </tr>
            </thead>
        );
    }
}

export default ListHeader;