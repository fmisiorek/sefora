import React, { Component } from 'react';
import ListHeader from './ListHeader';
import ListBody from './ListBody';
import './FilmGrid.css';

class FilmGrid extends Component {

    render() {
        return (
            <table className='FilmGrid'>
                <ListHeader sortBy={this.props.sortBy} sort={this.props.sort} headers={this.props.headers} />
                <ListBody data={this.props.values} headers={this.props.headers} />
            </table>
        );
    }
}

export default FilmGrid;