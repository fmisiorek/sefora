import React, {Component} from 'react';

class ListRow extends Component {
    render() {
        return (
            <tr>
                {
                    this.props.headers.map(({field}, index) => {
                        return <td key={index}>{this.props.data[field]}</td>
                    }, this)
                }
            </tr>
        );
    }
}

export default ListRow;