import React, { Component } from 'react';
import ListRow from './ListRow';
import './ListBody.css';

class ListBody extends Component {
    render() {
        return (
            <tbody className='ListBody'>
                {this.props.data ? this.props.data.map(({value, id}) => {
                    return <ListRow key={id} data={value} headers={this.props.headers} />
                }) : ''}
            </tbody>
        );
    }
}

export default ListBody;