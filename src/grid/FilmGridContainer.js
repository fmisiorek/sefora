import React, { Component } from 'react';
import FilmGrid from './FilmGrid';
import SearchFilm from '../filters/SearchFilm';
import Range from '../filters/Range';

class FilmGridContainer extends Component {
    constructor() {
        super();
        this.state = { data: {}, headers: [], values: [], sortBy: { field: '', asc: false }, ranges: {} };
        this.setSortBy = this.setSortBy.bind(this);
        this.makeSearch = this.makeSearch.bind(this);
        this.setRange = this.setRange.bind(this);
    }

    setSortBy(name) {
        const isTheSameField = this.state.sortBy.field === name;
        const asc = isTheSameField ? !this.state.sortBy.asc : false;
        this.setState({ sortBy: { field: name, asc: asc } });
        this.invokeSorting(name, asc);
    }

    isNumber(value) {
        if (value) {
            return !isNaN(Number(value));
        }

        return false;
    }

    invokeSorting(by = this.state.sortBy.field, asc = this.state.sortBy.asc) {
        this.state.values.sort((a, b) => {

            if (a.value[by] === undefined || a.value[by] === null) {
                return 1;
            }

            if (b.value[by] === undefined || b.value[by] === null) {
                return -1;
            }

            if (this.isNumber(a.value[by]) && this.isNumber(b.value[by])) {
                return a.value[by] - b.value[by];
            } else {
                return a.value[by].localeCompare(b.value[by]);
            }
        });

        if (!asc) {
            this.state.values.reverse();
        }
    }

    makeSearch(txt) {
        let values = this.state.data.values.filter((value) => {
            return value.name.toUpperCase().includes(txt.toUpperCase());
        });

        values = values.map((value, index) => {
            return { value: value, id: index }
        });

        this.setState({ values });
    }

    setRange(field, from, to) {
        const newRange = {};
        newRange[field] = { from, to };

        this.setState({ ranges: newRange }, this.filterByRange);
    }

    filterByRange() {
        const ranges = this.state.ranges;

        for (let key in ranges) {
            if (ranges.hasOwnProperty(key)) {
                let {from, to} = ranges[key];

                let values = this.state.data.values.filter((item) => {
                    const year = item.year;
                    if (from && to) {
                        return year >= from && year <= to;
                    } else {
                        if (from) {
                            return year >= from;
                        }

                        if (to) {
                            return year <= to;
                        }

                        return true;
                    }
                });

                values = values.map((value, index) => {
                    return { value: value, id: index }
                });

                this.setState({ values });

            }
        }

    }

    componentDidMount() {
        fetch(`https://api.myjson.com/bins/1tll6`)
            .then(result => result.json())
            .then(data => {
                this.setState({ data: data });

                let headers = data.fields.map((field, index) => {
                    return { name: field.label, field: field.field, id: index }
                })

                this.setState({ headers });

                let values = data.values.map((value, index) => {
                    return { value: value, id: index }
                });

                this.setState({ values });
            })
            .catch(err => console.log('Error during fetching and parsing data', err));
    }

    render() {
        this.invokeSorting();
        return (
            <div className='FilmGridContainer'>
                <SearchFilm search={this.makeSearch} />
                <Range range={this.setRange} name='Year' field='year' />
                <FilmGrid headers={this.state.headers} values={this.state.values} sortBy={this.state.sortBy} sort={this.setSortBy} />
            </div>
        );
    }
}

export default FilmGridContainer;